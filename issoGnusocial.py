#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pycurl
import sqlite3
import json
import argparse
import time
import socket
import urllib.parse
import feedparser
from configparser import SafeConfigParser
from xml.dom import minidom
from io import BytesIO
from os import listdir
from re import sub
from sys import exit
from sys import argv


class Database:
    """ Object to manage databases. """
    def __init__(self, database='/var/lib/isso/comments.db'):
        """
        Connect to the database.

        database -- string containig the filepath of the db
            (default: /var/lib/isso/comments.db)
        """

        self.connection = sqlite3.connect(database)

    def create_table(self):
        """Create table for isso-gnusocial."""

        current = self.connection.cursor()
        current.execute("DROP TABLE IF EXISTS archive")
        current.execute('CREATE TABLE archive(id INTEGER PRIMARY KEY, uri TEXT'
                        ', title TEXT, posted INT, created TEXT, notice_id INT'
                        ')')

    def insert(self, param):
        """
        Insert wrapper.

        Keyword arguments:
        param -- string containing the formatted insert query.
        """

        self.connection.execute(param[0], param[1])
        self.connection.commit()

    def select(self, param):
        """
        Select wrapper.

        Keyword arguments:
        param -- string containing a sql select
        """

        current = self.connection.cursor()
        current.execute(param)
        rows = current.fetchall()

        return rows

    def update(self, param):
        """
        Update only the created column.

        Keyword arguments:
        param -- list containing all the values
         """

        current = self.connection.cursor()
        current.execute(param)
        self.connection.commit()

    def close(self):
        """Close the database connection."""

        self.connection.close()


class issoGnusocial:
    """Main class of the program."""

    def __init__(self):
        """Create the object."""
        self.db = Database()

    def get_config(self, section):
        """
        Parse config file and return it on a list.

        Keyword arguments:
        section -- string containing the section to take from the config file
        """

        config = []
        parser = SafeConfigParser()
        parser.read("config.ini")

        for name, value in parser.items(section):
            config.append(value)

        return config

    def get_new_post(self, admin_config):
        """
        Query the database to see if there are new posts and return it on a
        list.

        Keyword arguments:
        admin_config -- list containing the admin section to take from the
            config file
        """

        new = []
        transition = []
        archive = self.db.select("SELECT * FROM archive")

        # Pass from tuple to list to make it easier to compare
        for i in archive:
            transition.append(i[1])

        feed = feedparser.parse(admin_config[3])
        entries_keys = list(feed.entries[0].keys())

        for article in feed.entries:
            uri = urllib.parse.urlparse(article.link).path
            title = article.title.encode('utf8').decode('latin1')

            if uri not in transition:
                new.append([title, uri])

        return new

    def shortener(self, post):
        """
        Return a shortened url.

        Keyword argument:
        post -- string containing a url to be shortened
        """

        api = ('http://qttr.at/yourls-api.php?format=xml&action=shorturl'
               '&signature=b6afeec983&url=' + post)
        buffer = BytesIO()
        curl = pycurl.Curl()
        curl.setopt(pycurl.URL, api)
        curl.setopt(pycurl.VERBOSE, False)
        curl.setopt(pycurl.WRITEDATA, buffer)
        curl.perform()

        buffer = buffer.getvalue().decode('utf-8')

        xmldoc = minidom.parseString(buffer)
        item = xmldoc.getElementsByTagName('result')
        url = item.item(0).getElementsByTagName('shorturl')[0].firstChild.data

        return url

    def post(self, admin_config, gs_config, article):
        """
        Use pycurl object to make a POST query and publish the new article and
        its url. When it finishs posting, it parses the XML response to take
        the notice id and saves it on the database.

        Keyword argument:
        admin_config -- list containing the administration configuration
        gs_config -- list containing the GNU social configuration
        article -- list containing the uri's post and it's title
        """

        social_url = gs_config[0]
        user = gs_config[1]
        password = gs_config[2]
        website = admin_config[2]
        title = article[0].encode('latin1').decode('utf8')
        url = website + article[1]
        buffer = BytesIO()
        uri = article[1]

        if admin_config[2] == 'yes':
            url = self.shortener(url)
        url = urllib.parse.unquote(url)

        post_data = {'status': title + ' - ' + url}
        postfields = urllib.parse.urlencode(post_data)

        # Prepare pycurl object
        curl = pycurl.Curl()
        curl.setopt(curl.USERPWD, user + ':' + password)
        curl.setopt(curl.POSTFIELDS, postfields)
        curl.setopt(curl.VERBOSE, False)
        curl.setopt(curl.WRITEDATA, buffer)
        curl.setopt(curl.URL, social_url + '/api/statuses/update.xml')

        insecure = 'yes'
        if insecure == 'yes':
            curl.setopt(pycurl.SSL_VERIFYPEER, 0)
            curl.setopt(pycurl.SSL_VERIFYHOST, 0)

        try:
            curl.perform()
        except Exception as e:
            print('There\'s an error: ' + str(e))
            curl.close()
            exit()

        curl.close()
        buffer = buffer.getvalue().decode('utf-8')
        xmldoc = minidom.parseString(buffer)
        item = xmldoc.getElementsByTagName('status')

        # Get notice's id
        id = item.item(0).getElementsByTagName('statusnet:conversation_id'
                                               )[0].firstChild.data

        # Get notice's creation date and hour
        created = item.item(0).getElementsByTagName('created_at'
                                                    )[0].firstChild.data
        created = created.split()
        # Change the created_at's stupid format to atom's
        dict_change = {
            'Jan': '01', 'Feb': '02', 'Mar': '03', 'Apr': '04',
            'May': '05', 'Jun': '06', 'Jul': '07', 'Aug': '08',
            'Sep': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12'
            }

        mes = dict_change[created[1]]
        created = created[5] + "-" + mes + "-" + created[2] + \
            "T" + created[3] + created[4][:3] + ":" + created[4][3:]
        title = article[0].replace(' ', '+')

        self.db.insert(['INSERT INTO archive(uri, title, posted, created, '
                        'notice_id) VALUES(?,?,?,?,?)', [uri, title, '1',
                                                         created, id]])

    def get_gs_conversation(self, admin_config, gs_config, posted):
        """
        Use pycurl object to make a GET query and get the conversation from GS
        on atom format. Use the <updated> tag to compare and see if there's new
        comments. If new comments, return them with their notices id, reply id
        and conversation id.

        Keyword argument:
        admin_config -- list containing the administration configuration
        gs_config -- list containing the GNU social configuration
        posted -- list containing posted notices attributes
        """

        social_url = gs_config[0]
        user = gs_config[1]
        password = gs_config[2]
        post_id = str(posted[5])
        buffer = BytesIO()

        n_ids = []
        c_ids = []
        reply_tos = []
        comments = []

        # Prepare pycurl object
        curl = pycurl.Curl()
        curl.setopt(curl.USERPWD, user + ':' + password)
        curl.setopt(curl.VERBOSE, False)
        curl.setopt(curl.WRITEDATA, buffer)
        curl.setopt(curl.URL, social_url + '/api/statusnet/conversation/' +
                    post_id + '.atom?count=1000')

        # TODO add this as an option
        insecure = 'yes'
        if insecure == 'yes':
            curl.setopt(pycurl.SSL_VERIFYPEER, 0)
            curl.setopt(pycurl.SSL_VERIFYHOST, 0)

        try:
            curl.perform()
        except Exception as e:
            print('There\'s an error: ' + str(e))
            curl.close()
            exit()

        curl.close()
        buffer = buffer.getvalue().decode('utf-8')

        xmldoc = minidom.parseString(buffer)
        status = xmldoc.getElementsByTagName('entry')
        item = xmldoc.getElementsByTagName('feed')

        created = status.item(0).getElementsByTagName('updated'
                                                      )[0].firstChild.data
        last_created = self.db.select('select created from archive where '
                                      'posted not null and notice_id = ' +
                                      str(posted[5]))[0][0]
        conversation = {}
        # proceed only if the conversation feed is newer than the one stored
        # if created not in last_created:
        for node in status:
            user_list = node.getElementsByTagName('name')
            content_list = node.getElementsByTagName('content')
            uri_list = node.getElementsByTagName('uri')
            id_list = node.getElementsByTagName('status_net')
            reply_list = node.getElementsByTagName('link')

            user = user_list[0].firstChild.data.split('@')[0]
            content = content_list[0].firstChild.data
            website = uri_list[0].firstChild.data
            n_id = id_list[0].attributes['notice_id'].value

            for i in reply_list:
                if i.attributes['rel'].value == "related":
                    reply_to = i.attributes['href'].value.split('/')[-1]
                    break
                else:
                    # won't be ever used, but just in case
                    reply_to = None

            c_id = item.item(0).getElementsByTagName('id')[0].firstChild.data
            c_id = c_id.split('/')[-1].split('.')[0]
            email = str(user) + '@' + website.split('/')[2]
            comment = [str(website), user, email, str(content), c_id, n_id, reply_to, created]

            comments.append(comment)

        return comments

    def add_parent(self, all_comments, current_comment):
        """
        If new comments are replies to other comments, update the existing.

        Keyword arguments:

        old_comments -- list containing all saved comments
        n_ids -- list containing all the notice's id
        p
        """

        parent_id = all_comments[-1][5]

        n_ids = []
        for n in all_comments:
            n_ids.append(n[5])

        for comment in all_comments:
            reply_to = comment[6]

            if reply_to in n_ids and reply_to != parent_id and set(comment) == set(current_comment):
                comment_to_reply = all_comments[n_ids.index(reply_to)]

                reply_id = self.db.select('select id from comments where '
                                              'author is \'' +
                                              comment_to_reply[1] + '\' and '
                                              'text is \'' +
                                              comment_to_reply[3] + '\'')[0][0]

                self.db.update('update comments set parent = \'' +
                                str(reply_id) + '\' where author is \'' +
                                current_comment[1] + '\' and text is \'' +
                                current_comment[3] + '\'')

    def add_parent_gs(self, comment, gs_config, admin_config,  notice):
        conversation = self.get_gs_conversation(admin_config, gs_config, notice)

        n = self.db.select("select author,text from comments where id = " + str(comment[4]))

        for comment in conversation:
            user = comment[1]
            text = comment[3]

            for c in n:
                if (c[0] == user and c[1] == text) or (text == c[0] + " dice: " + c[1]):
                    reply = comment[5]
                    break
                else:
                    reply = ""
            if reply:
                break

        return reply

    def new_comment(self, comment):
        """
        See if passed comments are yet posted to database, if not return them.

        Keyword arguments:

        comment -- string containing one comment
        c_id -- int containing the conversation's id
        """

        new = []
        saved = []
        c_id = comment[4]

        title = self.db.select('select title from archive where '
                               'notice_id = \'' + c_id + '\'')

        title = title[0][0]
        tid = self.db.select('select id from threads where '
                             'title = \'' + title + '\'')

        # if there's no prior comment thread, return comments since all are
        # new
        if not tid:
            return comment

        tid = str(tid[0][0])
        posted = self.db.select('select website,author,email,text from '
                                'comments where tid = ' + tid)
        i = 0

        # change from list of tuples to lists of lists
        for post in posted:
            saved.append(list(post))

        t_comment = [comment[0], comment[1], comment[2], comment[3]]

        for clean in saved:
            if set(clean) != set(t_comment) and clean[1] + " dice: " + clean[3] not in t_comment[3]:
                if clean[1] + " dice: " != "Anonymous dice: ":
                    new = comment

            else:
                new = []
                break

        return new

    def post_comments(self, notice, comment, created):
        """
        Post comments to isso from GNU social.

        Keyword arguments:
        notice -- list containing all the post attributes
        comment -- list containing all the comment attributes
        created -- string containing the section to take from the config file
        """

        uri = notice[1]
        title = notice[2]
        mode = str(1)  # no fucking idea what the mode is
        gs_server = comment[2].split('@')[1]
        remote_addr = socket.gethostbyname_ex(gs_server)[2][0]
        text = comment[3]
        email = comment[2]
        author = comment[1]
        website = comment[0]

        t_exists = self.db.select('SELECT uri from threads WHERE uri is \'' +
                                  uri + '\'')
        # if there's no comments thread, create it
        if not t_exists:
            self.db.insert(['INSERT INTO threads(uri, title) VALUES(?,?)',
                            [uri, title]])

        timestruct = time.strptime(created, "%Y-%m-%dT%H:%M:%S+00:00")
        date = time.mktime(timestruct)

        tid = str(self.db.select('select id from threads where uri is \'' +
                                 uri + '\'')[0][0])

        self.db.insert(['INSERT INTO comments(tid, created, mode, remote_addr,'
                        ' text, author, email, website, voters) VALUES(?,?'
                        ',?,?,?,?,?,?,?)', [int(tid), date, mode, remote_addr,
                                            text, author, email,
                                            website, 0]])
        self.db.update('UPDATE archive SET created = \'' + created + '\' WHERE'
                       ' uri = \'' + uri + '\'')

    def post_comment_gs(self, jid, password, notice, reply):
        user = jid.split('@')[0]
        social_url = 'https://' + jid.split('@')[1]

        post_data = {'status': notice, 'in_reply_to_status_id': reply}
        postfields = urllib.parse.urlencode(post_data)

        # Prepare pycurl object
        buffer = BytesIO()
        curl = pycurl.Curl()
        curl.setopt(curl.USERPWD, user + ':' + password)
        curl.setopt(curl.VERBOSE, False)
        curl.setopt(curl.WRITEDATA, buffer)
        curl.setopt(curl.URL, social_url + '/api/statuses/update.xml')
        curl.setopt(curl.POSTFIELDS, postfields)

        insecure = 'yes'
        if insecure == 'yes':
            curl.setopt(pycurl.SSL_VERIFYPEER, 0)
            curl.setopt(pycurl.SSL_VERIFYHOST, 0)

        try:
            curl.perform()
        except Exception as e:
            print('There\'s an error: ' + str(e))
            curl.close()
            exit()

        curl.close()
        buffer = buffer.getvalue().decode('utf-8')
        # print(buffer)
        xmldoc = minidom.parseString(buffer)
        item = xmldoc.getElementsByTagName('status')

        created = item.item(0).getElementsByTagName('created_at'
                                                    )[0].firstChild.data

        return created

class ParseOptions():
    """Parse command line options of this program."""
    def __init__(self):
        parser = argparse.ArgumentParser(description='', prog='isso-gnusocial')
        parser.add_argument('-p', '--post', dest='post', action='store_true',
                            help='posts new article to GNU social')
        parser.add_argument('-i', '--install', dest='install',
                            action='store_true', help='create initial '
                            'config')
        parser.add_argument('-f', '--follow-up', dest='follow_up',
                            action='store_true', help='see if new '
                            'comments')

        self.args = parser.parse_args()
        # Make all options accesible within self
        self.post = self.args.post
        self.install = self.args.install
        self.follow_up = self.args.follow_up

        self.parser = parser
        self.db = Database()
        self.issogs = issoGnusocial()
        self.admin_config = self.issogs.get_config('admin')
        self.gs_config = self.issogs.get_config('gnu_social')

    def pointers(self):
        """This are the options of the program."""

        if self.post:
            new = self.issogs.get_new_post(self.admin_config)

            if new:
                for post in new:
                    self.issogs.post(self.admin_config, self.gs_config, post)
            self.db.close()

        elif self.install:
            feed = self.admin_config[3]
            self.db.create_table()

            feed = feedparser.parse(feed)
            entries_keys = list(feed.entries[0].keys())

            for article in feed.entries:
                uri = urllib.parse.urlparse(article.link).path
                title = article.title.encode('utf8'
                                             ).decode('latin1'
                                                      ).replace(
                                                          ' ', '+')

                self.db.insert(['INSERT INTO archive(uri, title, posted, '
                                'created, notice_id) VALUES(?, ?, ?, ?, ?'
                                ')', [uri, title, '0', None, None]])
            self.db.close()

        elif self.follow_up:
            posted = self.db.select('select * from archive where posted is not'
                                    ' 0;')

            for notice in posted:
                comments = []
                new = []
                i = 0

                sel = self.db.select('select id,uri from threads where uri is \'' +
                                          notice[1] + '\'')

                conversation = self.issogs.get_gs_conversation(self.admin_config,
                                                          self.gs_config,
                                                          notice)
                # gs_comments = conversation[0]
                last_created = self.db.select('select created from archive where '
                                        'posted not null and notice_id = ' +
                                        str(notice[5]))[0][0]

                # coger reply de bbdd, si no hay es = al hilo padre (osea que no tendra)
                # si hay, ver si el text del reply esta en gs_comments o en la bbdd , si esta meterlo en el post_gs_comments
                # if there's any comment...
                if sel:
                    print("entro en blog a gs")
                    tid = str(sel[0][0])
                    uri = sel[0][1]

                    isso_comments = self.db.select('select * from comments where '
                                                   'tid = ' + tid)

                    timestruct = time.strptime(conversation[0][-1], "%Y-%m-%dT%H:%M:%S+00:00")
                    feed_time = time.mktime(timestruct)

                    jid = self.gs_config[3] + '@' + self.gs_config[0].split('/')[2]
                    password = self.gs_config[4]

                    cmt = ""
                    for com in isso_comments:
                        t_com = [com[10], com[8], com[9], com[7]]
                        if com[8] is None:
                            t_com[1] = "Anonymous"

                        for feed_com in conversation:
                            f_com = [feed_com[0], feed_com[1], feed_com[2], feed_com[3]]

                            if set(t_com) != set(f_com) and t_com[1] + " dice: " \
                                + t_com[3] not in f_com[1] + " dice: " + f_com[3]:
                                cmt = t_com

                            else:
                                cmt = ""
                                break

                        if cmt:
                            comments.append(cmt)
                            comments[-1].append(com[2])
                            comments[-1].append(feed_com[5])

                    if comments:
                        print("posteo a gs")
                        for comment in comments:
                            post = comment[1] + " dice: " + comment[3]
                            # tiene que ser conversation[6] no conversation[5]

                            # si no hay reply, usa el parent id
                            if comment[4]:
                                reply = self.issogs.add_parent_gs(comment,
                                                                  self.gs_config,
                                                                  self.admin_config,
                                                                  notice)
                            else:
                                reply = comment[5]

                            t_feed = self.issogs.post_comment_gs(jid, password, post, reply)

                            # prevent protection for posting to quick
                            time.sleep(5)

                comments = []

                # comprueba si el feed de la conversacion de gs se ha actualizado
                if conversation[0][-1] not in last_created:
                    print("entro en gs a blog")
                    for com in conversation:
                        cmt = self.issogs.new_comment(com)

                        if cmt:
                            comments.append(cmt)

                    if comments:
                        # delete the last comment, that is the post itself
                        del comments[len(comments) - 1]

                        for comment in reversed(comments):
                            print("posteo a la web")
                            post = self.issogs.post_comments(notice, comment,
                                                           conversation[0][-1])

                        for comment in comments:
                            self.issogs.add_parent(conversation, comment)

            self.db.close()

        elif len(argv) == 1:
            self.parser.print_help()


if __name__ == "__main__":
    options = ParseOptions()
    options.pointers()
