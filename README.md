Under heavy development, it shouldn't be used yet.

    $ python issoGnusocial.py -h
    usage: isso-gnusocial [-h] [-p] [-i] [-f]

    optional arguments:
      -h, --help       show this help message and exit
      -p, --post       posts new article to GNU social
      -i, --install    create initial config
      -f, --follow-up  see if new comments
